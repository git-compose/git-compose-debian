# Compose for Git .deb package

This repository contains information necessary to build
and publish .deb package with `gico` utility for easier 
installation and management in linux distributives, which
rely on apt-get repository structure.

Compose for Git is a tool to manage several repositories at once. 
Read more about it in https://gitlab.com/git-compose/git-compose

## Package content

So far package only have /usr/bin/gico file. It's a binary
suitable only for amd64 architecture, there's no i386 
or other architectures supported so far.

## Repository

Repository is prepared by standard dpkg tools and exposed 
via https using Gitlab Pages.

## Usage

Firstly you have to add `.list` file in apt-get configuration.

Easy way to it is to run the following command in bash:

``` bash
sudo bash -c 'echo "deb [trusted=yes] https://git-compose.gitlab.io/git-compose-debian trusty main" > /etc/apt/sources.list.d/git-compose.list'
```

Now update your repositories index:
``` bash
sudo apt-get update
```

Finally install package:
``` bash
sudo apt-get install git-compose
```

Check what version is installed:
```
gico version
```

## Why `[trusted=yes]`?

It's exposed over `https://`, so no need to protect from 
man in the middle attacks additionally. Though eventually PGP
signing would also be added to protect from possible attacks on 
PKI, it's considered low prio right now. 

## Stability

Currently git-compose is of 0.x.x versions, so no guarantees.
Also repository is not going to keep old versions until after 1.0.0.

