# Local Development

This folder contain everything needed to develop debian package locally.

docker-compose.yaml describes how to run docker container with all
necessary debian tools preinstalled and a few additional utils

All you need to do to run environment is to run following:

``` bash
docker-compose up -d
docker-compose exec packer bash
```

Firstly it would take some time to initialize. 
After that you'd be located in `/mydev/gico` folder and could for example run
`rebuild` to make dpkg rebuild package.

