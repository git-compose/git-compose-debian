#!/bin/bash
# Copyright (c) 2020 Timur Sultanaev All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.


# source /vol/local/.devutils.sh && initdev

build () {
    dpkg-buildpackage --host-arch amd64
    dpkg -c ../git-compose_*.deb
}

inst() {
    dpkg -i ../git-compose_*.deb
}

copy_vol () {
    cp /vol/* ./ -r
}

utils () {
    source local/.devutils.sh
}

reload () {
    copy_vol
    utils
}

rebuild () {
    reload 
    build
}

initdev () {
    mkdir -p /mydev/gico
    cd /mydev/gico
    copy_vol
    ./pack-amd64.sh
}