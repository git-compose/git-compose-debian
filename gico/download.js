/**
 * Copyright (c) 2020 Timur Sultanaev All rights reserved.
 * Use of this source code is governed by a BSD-style
 * license that can be found in the LICENSE file.
 */

const https = require('https');
const fs = require('fs');

if (!fs.existsSync("./downloaded")) fs.mkdirSync("./downloaded");

function downloadBinary(os, shortOs, links, ext, tag) {
    const name = `gico_${shortOs}_${tag}`
    const url = links.filter(it => it.name === name)[0].url
    console.log("Downloading from " + url)
    const file = fs.createWriteStream(`./downloaded/gico${ext}`);
    const request = https.get(url, response => {
        response.pipe(file);
    });
}


https.get("https://gitlab.com/api/v4/projects/17475748/releases?per_page=1",
    (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });
        resp.on('end', () => {
            const releases = JSON.parse(data)
            const release = releases[0]
            const tag = release.tag_name
            console.log("Have release: " + tag)
            fs.writeFile('release.txt', tag, function (err) {
                if (err) console.log(err);
            });
            const links = release.assets.links
            downloadBinary("linux", "linux", links, "", tag)
        });
    })
