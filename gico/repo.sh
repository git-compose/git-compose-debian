#!/bin/bash
# Copyright (c) 2020 Timur Sultanaev All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.


set -e
mkdir -p /repo
cp ../git-compose_*.deb /repo/
cd /repo
mkdir -p pool/main/n dists/trusty/main/binary-amd64
mv *.deb pool/main/n
dpkg-scanpackages -m pool | gzip > dists/trusty/main/binary-amd64/Packages.gz
dpkg-scanpackages -m pool > dists/trusty/main/binary-amd64/Packages
zcat dists/trusty/main/binary-amd64/Packages.gz 
find