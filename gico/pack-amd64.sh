#!/bin/bash
# Copyright (c) 2020 Timur Sultanaev All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

set -e
mkdir -p files/usr/bin

nodejs download.js

export gico_release="$(cat release.txt | sed 's/^v//g')"
echo "Downloaded ${gico_release}"

echo "Generating docs..."
chmod +x downloaded/gico
downloaded/gico doc man > doc/man/gico.8

cp downloaded/gico files/usr/bin/gico
cat debian/changelog | envsubst '${gico_release}' > .processed_changelog
cp .processed_changelog debian/changelog
#curl -o files/usr/bin/gico https://gitlab.com/api/v4/projects/17475748/jobs/594401809/artifacts/gico

chmod -x debian/install
chmod -x debian/manpages
dpkg-buildpackage --host-arch amd64
dpkg -c ../git-compose_*.deb

echo "Installing gico..."
dpkg -i ../git-compose_*.deb